import java.util.*;

public class Tamanho {
    public static final int minHeight=140;
    public static final int superTall=300;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Please enter your height: ");
        int height = scanner.nextInt();
        scanner.close();

        if(height>minHeight && height<superTall){
            System.out.println("You can ride the roller coaster.");
        }else if(height>superTall){
            System.out.println("You are super tall.");
        }else{
            System.out.println("You cannot ride the roller coaster.");       
        }
        
    }
}