import java.util.*;

public class Arrays {
    
    public static double sum(int[] numbers){
        double sum =0;
        for(int i=0;i<numbers.length;i++){
            sum+=numbers[i];
        }
        return sum;
    }

    public static double average(int[] numbers){
        return sum(numbers)/numbers.length;
    }

    public static double max(int[] numbers){
        double max =Double.MIN_VALUE;
        for(int i=0;i<numbers.length;i++){
            if(max<numbers[i]){
                max = numbers[i];
            }
        }
        return max;
    }
    public static double min(int[] numbers){
        double min =Double.MAX_VALUE;
        for(int i=0;i<numbers.length;i++){
            if(min>numbers[i]){
                min = numbers[i];
            }
        }
        return min;
    }    

    public static double difference(int[] numbers){
        return max(numbers)-min(numbers);
    }    

    public static double[] stats(int[] numbers){
        double[] result = {
            sum(numbers),
            average(numbers),
            max(numbers),
            min(numbers),
            difference(numbers)
        };
        return result;
    }

    public static void main(String args[]){
        double[] result = stats(new int[] {1,2,3,4,5,6,7});
        for(int i=0;i<result.length;i++){
            System.out.println(result[i]);
        }
    }
 }