public class Student {
    private String name;
    private int note;

    public String getName(){
        return name;
    }

    public int getNote(){
        return note;
    }

    public boolean passed(){
        if(note>=10){
            return true;
        }else{
            return false;
        }
    }

    public Student(String name,int note){
        this.name= name;
        this.note= note;
    }

}