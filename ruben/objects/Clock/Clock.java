public class Clock {

    //hours (range 0 - 23), minutes (range 0 - 59),seconds (range 0 - 59)
    private int hours;
    private int minutes;
    private int seconds;

    public void setClock(int seconds){
        this.hours = seconds / 3600;
        this.minutes = (seconds - (this.hours * 3600)) / 60;
        this.seconds = seconds - (this.hours * 3600) - (this.minutes * 60);
    }

    public void tick(){
        if(this.seconds+1>59){
            if(this.minutes+1>59){
                if(this.hours+1>23){
                    this.seconds=0;
                    this.minutes=0;
                    this.hours = 0;
                }else{
                    this.seconds=0;
                    this.minutes=0;
                    this.hours++;
                }
            }else{
                this.seconds=0;
                this.minutes++;
            }
        }else{
            this.seconds++;
        }
    }

    public void tickDown(){
        if(this.seconds-1<0){
            if(this.minutes-1<0){
                if(this.hours-1<0){
                    this.seconds=59;
                    this.minutes=59;
                    this.hours = 23;
                }else{
                    this.seconds=59;
                    this.minutes=59;
                    this.hours--;
                }
            }else{
                this.seconds=59;
                this.minutes--;
            }
        }else{
            this.seconds--;
        }
    }

    public void addClock(Clock clock){
        int seconds = (clock.getHours()*3600)+(clock.getMinutes()*60)+clock.getSeconds();
        for(int i=0; i<seconds;i++){
            this.tick();
        }
    }

    public Clock subtractClock(Clock clock){
        int seconds = (clock.getHours()*3600)+(clock.getMinutes()*60)+clock.getSeconds();
        int seconds1 = (this.hours*3600)+(this.minutes*60)+this.seconds;
        Clock diference;
        if(seconds>seconds1){
            diference = new Clock(seconds-seconds1);
        }else{
            diference = new Clock(seconds1-seconds);
        }
        return diference;
    }

    public String toString(){
        return String.format("(%02d:%02d:%02d)",this.hours,this.minutes,this.seconds);
    }

    public void setHours(int hours) {this.hours = hours; }

    public void setMinutes(int minutes) {this.minutes = minutes; }

    public void setSeconds(int seconds) {this.seconds = seconds; }

    public int getHours() {return hours; }

    public int getMinutes() {return minutes; }

    public int getSeconds() {return seconds; }

    public Clock(){
        this.hours = 12;
        this.minutes = 0;
        this.seconds = 0;
    }

    public Clock(int hours, int minutes, int seconds){
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    public Clock(int seconds){
        this.hours = seconds / 3600;
        this.minutes = (seconds - (this.hours * 3600)) / 60;
        this.seconds = seconds - (this.hours * 3600) - (this.minutes * 60);
    }
}
