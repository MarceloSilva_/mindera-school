import java.util.Arrays;

public class Bank {
    private Account[] accounts = new Account[1];
    private static int n;

    public void add(Account acc){
        accounts = Arrays.copyOf(accounts,n+1);
        accounts[n] = acc;
        this.n++;
    }

    public Account getData(int i){
        return accounts[i];
        //return "[ Name: '" + accounts[i].getName() + "' , Balance: " + accounts[i].getBalance() + " ]";
    }

    public String getAllData(){
        String result= "";
        for (int i=0; i<n ; i++){
            result = result +"\n[ ID: "+ accounts[i].getId() +" , Name: '" + accounts[i].getName() + "' , Balance: " + accounts[i].getBalance() + " ]";
        }
        return result;
        //return "[ Name: '" + accounts[i].getName() + "' , Balance: " + accounts[i].getBalance() + " ]";
    }

    public static int count(){
        return n;
    }

}
