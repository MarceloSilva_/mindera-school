import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class NumbersGame {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //Menu do jogo
        System.out.print(
            "------------- Numbers Game ------------\n\n 1 - User against PC number \n 2 - PC number against user\n\n"
        );
        int menuChoice = scanner.nextInt();

        switch (menuChoice) {
            case 1:
                numberPC();
                break;
            case 2:
                numberUser();
                break;
        }

        scanner.close();
    }
    
    public static void numberPC() {
        System.out.print("\033\143");
        Scanner scanner = new Scanner(System.in);

        System.out.println("Game - PC will guess your number \n\nInsert the number: ");
        int userNumber = scanner.nextInt();

        int pcNumber = 0;
        int start = 1;
        int end = 101;

        do {
            ThreadLocalRandom.current().nextInt(start, end);
            pcNumber = generate(start, end);

            if (pcNumber < userNumber) {
                start = pcNumber;
            } else if (pcNumber > userNumber) {
                end = pcNumber;
            }

            System.out.printf("\nPC Number: %d", pcNumber);
        } while (pcNumber != userNumber);
        
        System.out.println("\n\nThe computer guessed your number ! :(");

        scanner.close();
    }

    public static int generate(int start, int end) {
        return ThreadLocalRandom.current().nextInt(start, end);
    }

    public static void numberUser() {
        System.out.print("\033\143");
        Scanner scanner = new Scanner(System.in);

        int secretNumber = ThreadLocalRandom.current().nextInt(1, 101);
        int answerNumber = 0;
        int trys = 0;
        
        System.out.println("Game - Guess the PC number \n");

        do {
            System.out.println("Your guess: ");
            answerNumber = scanner.nextInt();
            
            if (answerNumber < secretNumber) {
                System.out.println("Go up!\n");
                trys++;
            } else if (answerNumber > secretNumber) {
                System.out.println("Go down!\n");
                trys++;
            }

        } while (secretNumber != answerNumber);
        
        System.out.format("\nVictory || You tried %d times\n", trys);

        scanner.close();
    }
}