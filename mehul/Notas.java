import java.util.*;

public class Notas {

    public static int NUMBERSTUDENTS = 3;
    public static int MIN_NOTE = 0;
    public static int POSITIVE = 10;
    public static int MAX_NOTE = 20;

    public static void printAluno(String name,int note){
        System.out.println("    Name: " + name + " Note: " + note);
    }

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        int[] noteStudent= new int[NUMBERSTUDENTS];
        String[] nameStudent = new String[NUMBERSTUDENTS];
        for(int i=0;i<nameStudent.length;i++){
            System.out.print("\033\143"); //Limpra ecra
            System.out.print("Student Name: ");
            nameStudent[i] = scanner.next();
            do{
                System.out.print("Note: ");
                noteStudent[i] = scanner.nextInt();
            }while(noteStudent[i]<MIN_NOTE || noteStudent[i]>MAX_NOTE);
        }
        System.out.println("\033\143Passed:");
        boolean passed=false,fail=false;
        for(int i=0;i<nameStudent.length;i++){   
            if(noteStudent[i]>=POSITIVE){
                printAluno(nameStudent[i],noteStudent[i]);
                passed=true;
            }       
        }
        if(!passed){
            System.out.println("    None Passed");
        }
        System.out.println("Fail:");
        for(int i=0;i<nameStudent.length;i++){   
            if(noteStudent[i]<POSITIVE){
                printAluno(nameStudent[i],noteStudent[i]);
                fail=true;
            }       
        }
        if(!fail){
            System.out.println("    None Failed");
        }

    }
 }
