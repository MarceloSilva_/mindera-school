import java.util.*;

public class ex5 {
    public static void main(String[] args) {
        //Coding Challenge 5 - Arrays
        //ex1
        int[] ex1 = new int[10];
        ex1[0] = 0;
        ex1[1] = 1;
        ex1[2] = 2;
        ex1[3] = 3;
        ex1[4] = 4;
        ex1[5] = 5;
        ex1[6] = 6;
        ex1[7] = 7;
        ex1[8] = 8;
        ex1[9] = 9;
        
        for(int i=0;i<ex1.length;i++){
            System.out.println(ex1[i]);
        }
        System.out.println("-------------------");
        //ex2
        int[] ex2 = new int[20];
        for(int i=0;i<ex2.length;i++){
            ex2[i] = i+1;
        }

        for(int i=0;i<ex2.length;i++){
            System.out.println(ex2[i]);
        }
        System.out.println("-------------------");
        //ex3
        double[] ex3 = new double[5];
        ex3[0] = 70.90;
        ex3[1] = 60.40;
        ex3[2] = 14.99;
        ex3[3] = 99.73;
        ex3[4] = 12.00;
        double sumOfNumbs =0;
        for(int i=0;i<ex3.length;i++){
            sumOfNumbs+= ex3[i];
        }
        double average = sumOfNumbs/ex3.length;
        System.out.format("Sum: %.2f\n",sumOfNumbs);
        System.out.format("Average: %.2f\n",average);

        System.out.println("-------------------");
        //ex4
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter your top five favourite foods.\n");

        String[] foods = new String[3];

        for(int i=0;i<foods.length;i++){
            System.out.print("Food "+ (i+1)+": ");
            foods[i]= scanner.nextLine();
        }
        System.out.println("No more memory available.");
        System.out.println("Your favourite foods are: ");
        for(int i=0;i<foods.length;i++){
            System.out.println("  "+foods[i]);
        }
    }
}