import java.util.Scanner;

public class Translacao {

    public static void main(String[] args){

        int[] size = getSize();
        char[][] grid = new char[size[0]][size[1]]; //x y
        int[][] coords = read();
        drawDiagonal(coords,grid);
        print(grid);
        moveDiagonal(coords,grid);
        print(grid);

    }

    public static void moveDiagonal(int[][] coords, char[][] grid){
        Scanner scanner = new Scanner(System.in);
        int xStart=0,xEnd=0,yStart=0,yEnd=0;
        System.out.println("Move Diagonal: ");
        System.out.print("x: ");
        int i = scanner.nextInt();
        System.out.print("y: ");
        int j = scanner.nextInt();

        if(coords[0][1]<coords[1][1]){
            xStart = coords[0][0];yStart = coords[0][1];
            xEnd   = coords[1][0];yEnd   = coords[1][1];
        }else if(coords[0][1]>coords[1][1]){
            xStart = coords[1][0];yStart = coords[1][1];
            xEnd   = coords[0][0];yEnd   = coords[0][1];
        }

        for(int a=0;a<grid.length;a++){
            for(int b=0; b<grid[1].length; b++){
                grid[a][b] ='\u0000';
            }
        }

        if(xEnd>xStart){
            for (int y = yStart, x = xStart; y <= yEnd && x <= xEnd; y++, x++) {
                grid[y+j][x+i] = 'x';
            }
        }else{
            for (int y = yStart, x = xStart; y <= yEnd && x >= xEnd; y++, x--) {
                grid[y+j][x+i] = 'x';
            }
        }
    }

    public static void drawDiagonal(int[][] coords, char[][] grid) {  // DRAW THE DIAGONAL ON THE GRID

        int xStart=0,xEnd=0,yStart=0,yEnd=0;
        if(coords[0][1]<coords[1][1]){
            xStart = coords[0][0]; yStart = coords[0][1];
            xEnd   = coords[1][0]; yEnd   = coords[1][1];
        }else if(coords[0][1]>coords[1][1]){
            xStart = coords[1][0];yStart = coords[1][1];
            xEnd   = coords[0][0];yEnd   = coords[0][1];
        }

        if(Math.abs(xStart-xEnd)==Math.abs(yStart-yEnd)) {
            if(xEnd>xStart){
                for (int y = yStart, x = xStart; y <= yEnd && x <= xEnd; y++, x++) {
                    grid[y][x] = 'x';
                }
            }else{
                for (int y = yStart, x = xStart; y <= yEnd && x >= xEnd; y++, x--) {
                    grid[y][x] = 'x';
                }
            }
        }
    }

    public static int[] getSize(){ // GET SIZE OF THE GRID
        int[] size = new int[2];
        Scanner scanner = new Scanner(System.in);
        System.out.println("Define the Grid size: ");
        System.out.print("Height: ");
        size[0] = scanner.nextInt();
        System.out.print("Width: ");
        size[1] = scanner.nextInt();
        return size;
    }

    public static int[][] read() {  // GET COORDS FOR DIAGONAL
        int[][] coords = new int[2][2];
        Scanner scanner = new Scanner(System.in);
        System.out.println("First Point: ");
        System.out.print("x: ");
        coords[0][0] = scanner.nextInt();
        System.out.print("y: ");
        coords[0][1] = scanner.nextInt();


        System.out.println("Second Point: ");
        System.out.print("x: ");
        coords[1][0] = scanner.nextInt();
        System.out.print("y: ");
        coords[1][1] = scanner.nextInt();


        return coords;
    }

    public static void print(char[][] grid) {  // PRINT GRID
        for (int y = 0; y < grid.length; y++) {
            for (int x = 0; x < grid[1].length; x++) {
                System.out.print(" " + grid[y][x] +" |");
            }
            System.out.println();
        }
    }
}
