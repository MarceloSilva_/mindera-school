import java.util.*;
public class NotasComClass {

    public static void printAluno(String name,int note){
        System.out.println("    Name: " + name + " Note: " + note);
    }

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        Student[] s = new Student[10];
        int note;
        String name;
        for(int i=0;i<10;i++){
            System.out.print("\033\143Student Name: ");
            name = scanner.next();
            System.out.print("Note: ");
            note = scanner.nextInt();
            s[i] = new Student(name, note);
        }
        scanner.close();
        //System.out.print(s[0].getName());
        System.out.println("\033\143Passed:");
        boolean passed=false,fail=false;
        for(int i=0;i<s.length;i++){   
            if(s[i].passed()){
                printAluno(s[i].getName(),s[i].getNote());
                passed=true;
            }       
        }
        if(!passed){
            System.out.println("    None Passed");
        }
        System.out.println("Fail:");
        for(int a=0;a<s.length;a++){   
            if(!(s[a].passed())){
                printAluno(s[a].getName(),s[a].getNote());
                fail=true;
            }       
        }
        if(!fail){
            System.out.println("    None Failed");
        }
    }
 }
