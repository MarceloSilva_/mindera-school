import java.util.Scanner;

public class Level2_array {

    public static void main(String[] args){
        String[] skiResorts = {
                "Whistler Blackcomb","Squaw Valley","Brighton",
                "Snowmass","Sun Valley","Taos"
        };
        printString(skiResorts);
        System.out.println("---------------");

        int[] intArray = {2, 3, 5, 7, 8};
        int sum = sumArray(intArray);
        System.out.println("SUM: " + sum);
        System.out.println("---------------");

        int average = average(intArray);
        System.out.println("Average: " + average);
        System.out.println("---------------");

        checkValue(intArray);
        System.out.println("---------------");

        checkIndex(intArray);


    }

    public static void printString(String[] skiResorts){
        for (String resort: skiResorts) {
            System.out.println(resort);
        }
    }

    public static int sumArray(int[] intArray){
        int sum=0;
        for (int num: intArray) {
            sum+= num;
        }
        return sum;
    }

    public static int average(int[] intArray){
        int average= sumArray(intArray)/intArray.length;
        return average;
    }

    public static void checkValue(int[] intArray){
        Scanner scanner = new Scanner(System.in);
        boolean check = false;
        System.out.print("Value: ");
        int numSearch = scanner.nextInt();
        for (int num: intArray) {
            if(num==numSearch){
                check=true;
                break;
            }
        }
        if (check){
            System.out.println("Correct!");
        }else {
            System.out.println("Value not found");
        }
    }

    public static void checkIndex(int[] intArray){
        Scanner scanner = new Scanner(System.in);
        Integer index= null;

        System.out.print("Value: ");
        int numSearch = scanner.nextInt();
        for (int i=0;i < intArray.length; i++) {
            if(intArray[i]==numSearch){
                index=i;
                break;
            }
        }
        if(index != null){
            System.out.println("Found at Index: "+ index);
        }else {
            System.out.println("Value not found");
        }
    }
}
