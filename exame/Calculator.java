import java.util.Scanner;

public class Calculator {

    public static void main(String[] args){
        int firstNumber = readNumber();
        char operation = operation();
        int secondNumber = readNumber();

        double result = calculate(firstNumber,operation,secondNumber);

        System.out.println(result);

    }
    public static double calculate(int firstNumber,char operation,int secondNumber){
        switch (operation){
            case '+':
                return firstNumber+secondNumber;
            case '-':
                return firstNumber-secondNumber;
            case '/':
                return firstNumber/secondNumber;
            case '*':
                return firstNumber*secondNumber;
        }
        return 0;
    }
    public static int readNumber(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Number: ");
        int num = scanner.nextInt();
        return num;
    }

    public static char operation(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("+ , - , * , / or q (to quit)");

        char operation = scanner.next().charAt(0);
        if(operation=='q'){
            System.exit(0);
        }
        return operation;
    }
}
