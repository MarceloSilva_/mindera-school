//Write a Java program to find the maximum and minimum value of an array.
//
//Write a Java program to reverse an array of integer values.
public class Level3_array {

    public static void main(String[] args){
        int[] values= {1,2,3,4,5,6,7};

        findMaxMin(values);

        reverseArray(values);
    }

    static void reverseArray(int[] array){
        int limit = 0;

        if(array.length/2==0)
            limit= (array.length/2);
        else
            limit= (array.length/2)-1;

        for (int i=array.length-1,j=0; i > limit; i--,j++){
            int temp=array[j];
            array[j]=array[i];
            array[i]=temp;
        }
        System.out.print("Reversed Array: ");
        for (int num:array) {
            System.out.printf("%d ",num);
        }
    }

    static void findMaxMin(int[] array){
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        for (int num:array) {
            if(num>max)
                max=num;
            if(num<min)
                min=num;
        }

        System.out.printf("Max: %d || Min: %d \n",max,min);
    }
}
