import java.util.*;

public class Adivinha {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random r = new Random();
        int secret = r.nextInt(100) + 1;
        int guess;
        do{
            System.out.print("Write your guess: ");
            guess = scanner.nextInt();
            if(guess>secret){
                System.out.println("Your guess is higer");
            }else if(guess<secret){
                System.out.println("Your guess is lower");
            }
        }while(guess!=secret);
        System.out.println("Victory!");
    }
}