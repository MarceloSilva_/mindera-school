import java.util.*;

public class Multibanco {
    public static void main(String args[]){
        int[] notas = new int[4];//{5,10,20,50};
        int montante=1;

        int[] valores = {5,10,20,50};
        Scanner scanner = new Scanner(System.in);
        while(!(montante%5==0)){
            System.out.print("Valor a levantar:");
            montante = scanner.nextInt();
        }
        notas[3]= check(50,montante);
        montante-= 50*notas[3];
        notas[2]= check(20,montante);
        montante-= 20*notas[2];
        notas[1]= check(10,montante);
        montante-= 10*notas[1];
        notas[0]= check(5,montante);
        montante-= 5*notas[0];

        for(int i=0;i<notas.length;i++){
            System.out.println(notas[i]+" notas de "+ valores[i]);
        }
        scanner.close();
    }
    
    public static int check(int nota,int montante){
        int n=0;
        //System.out.println("CHECK -- MONTANTE: " + montante + " NOTA: "+nota);
        do{
            n++;
            montante-=nota;
            //System.out.println("CHECK -- "+ montante);
            if(montante==0){
                //System.out.println(n);
                return n;
            }
            if(montante<0){
                n--;
                montante+=nota;
                return n;
            }
        }while(montante>0 && montante>=nota);
        return n;
    }
 }