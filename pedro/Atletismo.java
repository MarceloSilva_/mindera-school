import java.util.*;

public class Atletismo {
    public static void main(String args[]){
        int precoM=1000;
        double larguraPista = 6.4;
        int larguraFutebol = 60;
        int comprimentoFutebol = 80;
        int raioCirculoInterior= larguraFutebol/2;
        double raioCirculoExterior = (larguraFutebol+(larguraPista*2))/2;

        //Calcular area do circulo atras das balizas
        double areaCirculoInterior= 3.14 * (raioCirculoInterior*raioCirculoInterior);
        //Calcular area do circulo exterior
        double areaCirculoExterior= 3.14 * (raioCirculoExterior * raioCirculoExterior);
        //Calcular area da pista
        double circularPista = areaCirculoExterior-areaCirculoInterior;
        double retoPista= (comprimentoFutebol*larguraPista)*2;

        double totalPista = circularPista+retoPista;
        double preco= precoM*totalPista;
        System.out.format("Preço total: %.2f €\n",preco);
    }
 }