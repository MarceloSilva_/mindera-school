/*
Given a matrix of 10 by 10, ask the user for 10 ships.
1 x ship with 4 squares
2 x ship with 3 squares
3 x ship with 2 squares
4 x ship with 1 square
For each ship, the user should input one or two coordinates, the beggining of the ship, and the end of the ship.
At the end print the board, and then, continuously ask the user for coordinates, as if bombs.
The program should terminate hence all ships are sunken.
*/

import java.util.*;

public class Ships {

    public static void main(String[] args) {
        int[][] grid = new int[10][10];
        print(grid);
        for (int i = 1; i < 11; i++) {
            if (i == 1) {
                System.out.println("Ship with 4 squares");
                drawShip(4, grid, i);
            } else if (i > 1 && i < 4) {
                System.out.println("Ship with 3 squares");
                drawShip(3, grid, i);
            } else if (i > 3 && i < 7) {
                System.out.println("Ship with 2 squares");
                drawShip(2, grid, i);
            } else if (i > 6 && i < 11) {
                System.out.println("Ship with 1 squares");
                drawShip(1, grid, i);
            }
        }
    }

    public static void drawShip(int size, int[][] grid, int ship) {
        int[][] coords = new int[2][2];
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("First Point: ");
            System.out.print("x: ");
            coords[0][0] = scanner.nextInt();
            System.out.print("y: ");
            coords[0][1] = scanner.nextInt();
            System.out.println("Second Point: ");
            System.out.print("x: ");
            coords[1][0] = scanner.nextInt();
            System.out.print("y: ");
            coords[1][1] = scanner.nextInt();
        } while (!checkValid(coords, grid, size, ship));
        print(grid);
    }

    public static boolean checkValid(int[][] coords, int[][] grid, int size, int ship) {
        int count = 0;
        //CHECK LIMITS
        if ((coords[0][0] >= 0 && coords[0][0] < grid.length) && (coords[0][1] >= 0 && coords[0][1] < grid.length) && (coords[1][0] >= 0 && coords[1][0] < grid.length) && (coords[1][1] >= 0 && coords[1][1] < grid.length)) {
            //CHECK SHIP
                // CHECK ROW
                if (coords[0][1] == coords[1][1]) {
                    if(checkRow(grid,coords[0][1],coords[0][0],size,ship)){
                        return true;
                    }else {
                        System.out.println("Choose a cord inside the grid");
                        return false;
                    }
                }
                // CHECK COLUMN
                else if (coords[0][0] == coords[1][0]) {
                    if(checkColumn(grid,coords[0][1],coords[0][0],size,ship)){
                        return true;
                    }else {
                        System.out.println("Choose a cord inside the grid......");
                        return false;
                    }
                }else{ return false;}
        } else {
            System.out.println("Choose a cord inside the grid");
            return false;
        }
    }


    public static boolean checkRow(int[][] grid, int y, int x,  int size, int ship) {
        System.out.println("CheckRow");
        int count=0;
        for (int i = x; i < x+size ; i++) {
            if (grid[y][i] == 0) {
                count++;
            }
            else {
                return false;
            }
        }
        if(count==size){
            for (int i = x; i < x+size ; i++) {
                grid[y][i] = ship;
            }
            return true;
        }
        return false;
    }

    public static boolean checkColumn(int[][] grid, int y, int x,  int size, int ship) {
        System.out.println("CheckColumn");
        int count=0;
        for (int i = y; i < y+size ; i++) {
            if (grid[i][x] == 0) {
                count++;
            }
            else {
                return false;
            }
        }
        if(count==size){
            for (int i = y; i < y+size ; i++) {
                grid[i][x] = ship;
            }
            return true;
        }
        return false;
    }

    public static void print(int[][] grid){
        for(int i=0; i<grid.length; i++){
            for (int j=0; j<grid[0].length; j++){
                System.out.print(" " + grid[i][j] + " |");
            }
            System.out.println();
        }
    }
}