import java.util.*;

public class ex4 {
    public static void main(String[] args) {
        //Coding Challenge 4 - Loops
        Scanner scanner = new Scanner(System.in);
        //Ex1
        for(int i=89;i<118;i++){
            System.out.println(i);
        }
        //Ex2
        int count=0;
        for(int i=1;i<41;i++){
            
            System.out.println(i);
            if(count==3){
                System.out.println("Quack");
                count=0;
            }
            count++;
        }
        //Ex3
        String password;
        do{
            System.out.print("Password: ");
            password = scanner.next();
        }while(!password.equals("shark50"));
            System.out.println("ACCESS APPROVED");

        //Ex4
        for(int i=1;i<11;i++){
            System.out.println(i);
        }
        scanner.close();
    }
}