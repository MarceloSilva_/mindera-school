//Write a Java program to find the common elements between two arrays (string values)
//
//Write a Java program to find the second largest element in an array.

import java.util.Arrays;

public class Level4_array {
    public static void main(String[] args) {
        String[] firstArray  = {"aaa", "bbb", "ccc"};
        String[] secondArray = {"aaa", "bbb", "ddd"};

        compareStrings(firstArray, secondArray);

        int[] intArray = {12, 5, 10, 15};
        findSecondGreatest(intArray);
    }


    static void findSecondGreatest(int[] intArray) {
        int max = Integer.MIN_VALUE, secondMax = 0;

        for (int num : intArray)
            if (num > max)
                max = num;

        for (int num : intArray)
            if (num < max && num > secondMax)
                secondMax = num;

        System.out.print("\n\n " + Arrays.toString(intArray) + "\n");
        System.out.printf("\nMax: %d || Second max: %d\n", max, secondMax);
    }


    static void compareStrings(String[] firstArray, String[] secondArray) {
        for (String string1 : firstArray)
            for (String string2 : secondArray)
                if (string1.equals(string2))
                    System.out.printf("\nEqual: %s = %s", string1, string2);
    }
}