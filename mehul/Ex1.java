import java.util.*;

public class Ex1 {
    public static void main(String args[]){
        Scanner scanner = new Scanner(System.in);
        int soma=0;
        int num;
        do{
            System.out.print("Numero: ");
            num = scanner.nextInt();
            if(num%2==0){
                soma+= num;
                System.out.println("Soma: " +soma);             
            }
        }while(num !=0); 
        scanner.close();
    }
 }