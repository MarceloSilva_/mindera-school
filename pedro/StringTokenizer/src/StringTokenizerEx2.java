import java.util.*;
public class StringTokenizerEx2 {

    public static void main(String[] args) {
        StringTokenizer isbn = new StringTokenizer("0 8 9 2 3 7 0 1 0 6");
        int[] x = new int[10];
        int counter=10;
        int resultado=0;
        while (isbn.hasMoreTokens()) {
            resultado += counter*Integer.parseInt(isbn.nextToken());
            counter--;
        }
        if(resultado%11==0){
            System.out.print("ISBN Correto");
        }else{
            System.out.print("ISBN Incorreto");
        }

    }
}
