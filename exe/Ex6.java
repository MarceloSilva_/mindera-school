import java.util.*;

public class Ex6 {
    public static boolean[] num = new boolean[9];
    public static void main(String[] args) {
        for(int a=111;a<999;a++){
            num = new boolean[9];
            int dobro = a*2;
            int triplo = a*3;
            if(!(triplo > 999) && ((a%10!=0)) && ((dobro%10!=0)) && ((triplo%10!=0))){
                //System.out.println("Numero: "+a + " D: "+dobro+" T: "+triplo);
                if(check(a) && check(dobro) && check(triplo)){
                    System.out.println("-----------");
                    System.out.println(a + " "+ dobro+ " "+triplo);
                    System.out.println("-----------");
                }
            }
        }
    }

    public static boolean check(int c){
        String number = String.valueOf(c);
        for(int i = 0; i < number.length(); i++) {
            int j = Character.digit(number.charAt(i), 10);
            if(j!=0){
                if(!num[j-1]){
                    //System.out.println("Novo: " +j);
                    num[j-1]=true;
                }else{
                    //System.out.println("Numero "+ j+ " repetido");
                    return false;
                }
            }else{return false;}
        }
        return true;
    }
}

