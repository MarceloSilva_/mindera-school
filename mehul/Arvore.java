import java.util.*;

public class Arvore {
    public static void main(String args[]){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Altura da arvore: ");
        int num= scanner.nextInt();
        for(int i=0;i<=num;i++){
            int estrelas = i*2+1;
            System.out.println();

            for(int x=i-num;x<num;x++){
                System.out.print(" ");
            }
            for(int nivel=0;nivel<estrelas;nivel++){
                System.out.print("*");
            }
            
        }
        System.out.println();
        
        scanner.close();
    }
 }

 /*       *
         ***
        *****
       *******
      *********



 */