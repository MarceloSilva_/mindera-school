import java.util.*;

public class Salario {
    public static final double OVERTIME=1.5;
    public static final double MINIMUMWAGE=8.00;
    public static final double MAXIMUMWORK=60;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double hours;
        double wage;
        double extraWage=0;
        double extraHours=0;
        do{
            System.out.print("Work hours: ");
            hours = scanner.nextInt();        
        }while(hours>MAXIMUMWORK);
        do{
            System.out.print("Wage/h: ");
            wage = scanner.nextInt();     
        }while(wage<MINIMUMWAGE);
        //Numeor de horas a mais
        if(hours>40){
            extraHours = (hours-40);
        }
        //Valor a acrescentar ao salario
        if(extraHours!=0){
            extraWage = (extraHours*wage)*OVERTIME;
        }
        //calcular salario
        double totalWage = (hours-extraHours)*wage+extraWage;

        scanner.close();
        System.out.println("Salary: "+totalWage);
        
    }
}