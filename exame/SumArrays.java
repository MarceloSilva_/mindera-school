import java.util.*;

public class SumArrays {

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Length of the array:");
        int length = scanner.nextInt();
        int[] arrayOne = populateArray(length);
        System.out.println();
        int[] arrayTwo = populateArray(length);
        int[] result = calculate(arrayOne,arrayTwo);

        for (int i=0; i<result.length; i++){
            System.out.print(" " +result[i]);
        }
    }

    public static int[] populateArray(int size){
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[size];
        for(int i=0; i<array.length; i++){
            System.out.print(i+1 + "º Number: ");
            array[i]= scanner.nextInt();
        }
        System.out.println();
        return array;
    }

    public static int[] calculate(int[] arrayOne,int[] arrayTwo){
        int[] result = new int[arrayOne.length];
        for(int i=0; i<arrayOne.length; i++){
            result[i]= arrayOne[i]+arrayTwo[i];
        }
        return result;
    }
}
