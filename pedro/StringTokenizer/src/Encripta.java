import java.util.*;
public class Encripta {
    public static int INCREMENT = 2;

    public static char cifra(char letter){
        char[] alpha_key= {'e','r','x','E','R','X'};
        boolean equal= false;
        for(int i=0; i<alpha_key.length; i++){
            //System.out.println("Comparar alpha_key");
            equal=false;
            if((letter==alpha_key[i])){
                //System.out.println("Letras igual");
                equal=true;
                break;
            }
        }
        if(!equal){
            letter+=INCREMENT;
        }

        return letter;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Phrase: ");


        while (!scanner.hasNext("[A-Za-z]+")) {
            System.out.println("Nope, that's not it!");
            scanner.next();
        }

        String phrase = scanner.next();
        StringTokenizer tokens = new StringTokenizer("Este exame vai correr bem");
        while (tokens.hasMoreTokens()){
            String word = tokens.nextToken();
            char[] wordArray = word.toCharArray();
            for (int i=0; i<wordArray.length; i++) {
                wordArray[i] = cifra(wordArray[i]);
                System.out.print(wordArray[i]);
            }
            //INCREMENT++;
            System.out.print(" ");
        }
    }
}

