import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class ex3 {
    public static void main(String[] args) {
        //Coding Challenge 3 - User Input
        Scanner scanner = new Scanner(System.in);
        //Ex1-2
        System.out.print("First Number: ");
        int x = scanner.nextInt();
        
        System.out.print("Second Number: ");
        int y = scanner.nextInt();

        if(x==y){
            System.out.format("%d and %d are the same\n",x,y);
        }else if(x>y){
            System.out.format("%d is bigger than %d\n",x,y);
            int difference = x-y;
            System.out.println("Difference between: "+ difference);
        }else {
            System.out.format("%d is bigger than %d\n",y,x);
            int difference = y-x;
            System.out.println("Difference between: "+ difference);
        }
        System.out.println("\n\n-------------------");
        //Ex3
        System.out.print("Select a number (1 to 5): ");
        int number = scanner.nextInt();



        int randomNumber = ThreadLocalRandom.current().nextInt(1, 6);
        if(number!=randomNumber){
            System.out.print("Select another number: ");
            number= scanner.nextInt();
            if(number!=randomNumber){
                System.out.println("Hard luck! Maybe next time.");
            }else{
                System.out.println("Correct! Aren’t you lucky.");
            }
        }else{
            System.out.println("Correct! Aren’t you lucky.");
        }
        scanner.close();
    }
}