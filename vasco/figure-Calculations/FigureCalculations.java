import java.util.*;

public class FigureCalculations {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Please enter the cube : ");
        int arestaCubo = scanner.nextInt();

        int volumeCubo = arestaCubo^3;

        System.out.print("Please enter the pyramid base: ");
        int basePiramide = scanner.nextInt();
        System.out.print("Please enter the pyramid height: ");
        int alturaPiramide = scanner.nextInt();

        scanner.close();
        int volumePiramide = (basePiramide * alturaPiramide) / 3;
        
        System.out.println(volumeCubo == volumePiramide);
        System.out.println(volumeCubo > volumePiramide);
    }
}