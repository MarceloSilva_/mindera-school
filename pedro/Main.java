import java.util.*;
public class Main {

    public static void main(String[] args) {

        String s = "A Maria compra uma camisa amarela";
        java.util.StringTokenizer tokens = new java.util.StringTokenizer(s);
        while (tokens.hasMoreTokens()) {
            String nextString = tokens.nextToken();
            char[] next = nextString.toCharArray();
            int a=0;
            for (char letra:next) {
                if(letra=='a' || letra=='A'){
                    a++;
                }
            }
            if(a>=2){
                System.out.println(nextString);
            }
        }

    }
}
