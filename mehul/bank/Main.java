import java.util.Scanner;

public class Main {
    public static Bank bank = new Bank();

    public static void main(String[] args){
        menu();
    }

    public static void selectAccount(){
        int id;
        Scanner sc = new Scanner(System.in);
        System.out.println("------ ACCOUNT ID ------");
        System.out.println("Account id: ");
        id = sc.nextInt();
        accountMenu(id);
    }

    public static void accountMenu(int id){
        Scanner sc = new Scanner(System.in);
        int opc;
        int amount;
        int tranferId;
        Account account = bank.getData(id);
        do {
            System.out.println("------ ACCOUNT OPTION ------");
            System.out.println("ID: " + account.getId() + " , Name: " + account.getName() + " , Balance: " + account.getBalance());
            System.out.println("1 - Withdraw");
            System.out.println("2 - Deposit");
            System.out.println("3 - Transfer");
            System.out.println("0 - Exit");
            System.out.print("Option: ");
            opc = sc.nextInt();
            switch (opc){
                case 1:
                    System.out.println("Amount: ");
                    amount = sc.nextInt();
                    if(account.withdraw(amount))
                        System.out.println("Successful withdraw");
                    else{
                        System.out.println("Error!!");
                    }
                    break;
                case 2:
                    System.out.println("Amount: ");
                    amount = sc.nextInt();
                    account.deposit(amount);
                    break;
                case 3:
                    System.out.println("Amount: ");
                    amount = sc.nextInt();
                    System.out.println("Account Id: ");
                    tranferId = sc.nextInt();
                    if(account.transfer(amount,bank.getData(tranferId)))
                        System.out.println("Successful transfer");
                    else{
                        System.out.println("Error!!");
                    }
                    break;
            }
        }while (opc!=0);
    }

    public static void listAll(){
        System.out.println(bank.getAllData());
        System.out.println("Total Accounts: "+ Bank.count());
    }

    public static void addAccount(){
        Scanner sc = new Scanner(System.in);
        String name;
        int balance;
        System.out.println("------ NEW ACCOUNT ------");
        System.out.print("Name: ");
        name= sc.nextLine();
        System.out.print("Balance: ");
        balance= sc.nextInt();
        Account acc = new Account(Bank.count(),name,balance);
        bank.add(acc);
    }

    public static void menu(){
        int opc;
        do{
            Scanner sc = new Scanner(System.in);
            System.out.println("----------------------");
            System.out.println("1 - Add Account");
            System.out.println("2 - List Accounts");
            System.out.println("3 - Select Account");
            System.out.println("0 - Exit");
            System.out.println("----------------------");
            System.out.print("Option: ");
            opc =sc.nextInt();
            switch (opc){
                case 1:
                    addAccount();
                    break;
                case 2:
                    listAll();
                    break;
                case 3:
                    selectAccount();
                    break;
            }
        }while (opc!=0);
    }
}
