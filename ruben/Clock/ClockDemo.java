import java.util.Scanner;

public class ClockDemo {

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("FIRST CLOCK");
        System.out.print("Seconds since midnight: ");
        int secondsFirst = sc.nextInt();
        Clock firstClock = new Clock(secondsFirst);

        for(int i=0; i<10;i++)
            firstClock.tick();

        System.out.println("SECOND CLOCK");
        System.out.print("Hours: ");
        int secondHours = sc.nextInt();
        System.out.print("Minutes: ");
        int secondMinutes = sc.nextInt();
        System.out.print("Seconds: ");
        int secondSeconds = sc.nextInt();
        Clock secondClock = new Clock(secondHours,secondMinutes,secondSeconds);

        for(int i=0; i<10;i++){
            secondClock.tick();
            System.out.println("Second Clock: " + secondClock.toString());
        }

        firstClock.addClock(secondClock);

        System.out.println("First  Clock: " + firstClock.toString());
        System.out.println("Second Clock: " + secondClock.toString());

        Clock thirdClock = firstClock.subtractClock(secondClock);

        System.out.println("Third Clock: " + thirdClock.toString());
    }
}
