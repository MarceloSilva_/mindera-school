import java.util.*;

public class Menor {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int smallest= Integer.MAX_VALUE;
        System.out.print("Please enter the amount of numbers: ");
        int x = scanner.nextInt();
        int number;
        for(int i=0;i<x;i++){
            System.out.print((i+1)+" Number: ");
            number = scanner.nextInt();
            if(number<smallest){
                smallest=number;
            }
        }
        System.out.println("Smallest number: "+smallest);
        scanner.close(); 
    }
}