import java.util.*;

public class ArvoreNumeros {
    public static void main(String args[]){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Altura da arvore: ");
        int maxRows = scanner.nextInt();
        int r, num;
        for (int i = 0; i <= maxRows; i++) {
            num = 1;
            r = i + 1; //nºlinha + 1
            for(int x=i-maxRows;x<maxRows;x++){
                System.out.print(" ");
            }
            for (int col = 0; col <= i; col++) {
                if (col > 0) {
                    num = num * (r - col) / col; //1 * (4 - 2) / 2 
                }
                System.out.printf("%d ",num);
            }
            System.out.println();
        }
        
        scanner.close();
    }
 }

 /*       1
         1 1
        1 2 1
       1 3 3 1
        *****
       *******
      *********



 */