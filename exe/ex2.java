import java.util.*;

public class ex2 {
    public static void main(String[] args) {
        //Coding Challenge 2 - User Input
        //Ex1
        Scanner scanner = new Scanner(System.in);

        System.out.print("First Name: ");
        String firstName = scanner.next();
        
        System.out.print("Age: ");
        int age = scanner.nextInt();
        
        System.out.print("Amount of money in your pocket: £");
        double money = scanner.nextDouble();
        
        System.out.println();
        
        double moneyNeeded = Math.ceil(money) - money;
        
        System.out.println("Your name is " + firstName + " and you are " + age + " years old.");
        System.out.format("You have £%.2f on you and need £%.2f.\n", money, moneyNeeded);
        System.out.format("You’ve lived %d years. In another %d years you’ll be "+ (age*2)+" years old.\n",age,age);
        System.out.println("Number of characters on your name: "+firstName.length());

        //Ex2
        System.out.println("\n\n-------------------");

        System.out.print("Bill total: ");
        double bill = scanner.nextDouble();
    
        System.out.print("Number of people: ");
        int numberPeople = scanner.nextInt();

        System.out.println();

        System.out.format("Total per person: %.2f\n", (bill/numberPeople));

        scanner.close();
    }
}