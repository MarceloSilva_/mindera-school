import java.util.*;

public class Tabuada {
    public static void main(String args[]){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Tabuada do: ");
        int num = scanner.nextInt();
/*
        for(int i=1;i<=10;i++){
            System.out.format("\n%dx%d=%d",num,i,(i*num));
        
        int i=1;
        while(i<=10){
            System.out.format("%dx%d=%d\n",num,i,(i*num));
            i++;
        }
        }*/
        int i=1;
        do{
            System.out.printf("%dx%02d=%d\n",num,i,(i*num));
            i++;
        }while(i<=10);
        scanner.close();
    }
 }