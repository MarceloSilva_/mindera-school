import java.util.*;
public class QuatroEmLinha {
    public static int MAXROWS    = 7;
    public static int MAXCOLUMNS = 6;
    public static boolean WIN    = false;
    public static void main(String[] args){
        int player = 1;
        int[][] grid = new int[MAXROWS][MAXCOLUMNS]; //y x
        int[] pos = new int[2];
        do {
            print(grid);
            if (player == 1){
                pos = add(grid, player);
                if (!(WIN = checkWIN(grid, player, pos[1], pos[0]))){
                    player = 2;
                }
            } else {
                pos = add(grid, player);
                if(!(WIN = checkWIN(grid, player, pos[1], pos[0]))){
                    player = 1;
                }
            }
        } while(!WIN);
        print(grid );
        System.out.print("\nCongratulation ");
        if (player == 1){
            System.out.print("\u001B[31m PLAYER 1");
        } else {
            System.out.print("\u001B[34m PLAYER 2");
        }
        // 0- CLEAR   1- PLAYER1   2- PLAYER2
    }
    public static void print(int[][] grid){
        for (int y = 0; y < grid.length; y++) {
            for(int x = 0;x < grid[1].length;x++)
                if (grid[y][x] == 1)
                    System.out.print("\u001B[31m 0 \u001B[0m|");
                else if (grid[y][x] == 2)
                    System.out.print("\u001B[34m 0 \u001B[0m|");
                else
                    System.out.print("\u001B[0m 0 |");
            System.out.println();
        }
    }
    public static int[] add(int[][] grid,  int player){
        int column      = read(grid, player);
        int row         = -1;
        boolean changed = false;
        int[] positions = new int[2];
        for(int y = grid[1].length; y >= 0 && !changed; y--) {
            if (grid[y][column] == 0){
                grid[y][column] = player;
                row = y;
                changed = true;
            }
        }
        positions[0] = row; positions[1] = column;
        return positions;
    }
    public static int read(int[][] grid, int player){
        int x;
        Scanner scanner = new Scanner(System.in);
        System.out.println("PLAYER " + player + ": ");
        do {
            System.out.print("Column: ");
            x = scanner.nextInt() - 1;
        } while(checkFull(grid, x));
        return x;
    }
    public static boolean checkFull(int[][] grid, int column) { return grid[0][column] != 0; }

    public static boolean checkWIN(int[][] grid, int player, int column, int row) {
        // System.out.println(column);
        if (checkRow(grid, player))
            return true;
        if (checkColumn(grid, player))
            return true;
        if(checkMajorDiagonal(grid, player, column, row))
            return true;
        if(checkMinorDiagonal(grid, player, column, row))
            return true;
        return false;
    }
    public static boolean checkRow(int[][]grid, int player) {
        for (int y = 6; y > 2; y--)
            for(int x = 0; x < grid[1].length; x++)
                if (grid[y][x] == player && grid[y-1][x] == player && grid[y-2][x] == player && grid[y-3][x] == player) // HORIZONTAL
                    return true;
        return false;
    }
    public static boolean checkColumn(int[][]grid, int player) {
        for (int y = 0; y < grid.length; y++)
            for (int x = 0; x < 3; x++)
                if (grid[y][x] == player && grid[y][x+1] == player && grid[y][x+2] == player && grid[y][x+3] == player) // VERTICAL
                    return true;
        return false;
    }
    public static boolean checkMajorDiagonal (int[][]grid, int player, int column, int row) {
        int count = 1;
        //check down major diagonal
        for(int x = column + 1, y = row + 1; y < grid.length && x < grid[0].length; x++, y++) {
            if (grid[y][x] == player)
                count ++;
            else
                break;
        }
        if(count >= 4)
            return true;
        //check up major diagonal
        for(int x = column - 1, y = row - 1; y >= 0 && x >= 0; x--, y--) {
            if (grid[y][x] == player)
                count ++;
            else
                break;
        }
        if(count >= 4)
            return true;
        return false;
    }
    public static boolean checkMinorDiagonal (int[][]grid, int player, int column, int row) {
        int count = 1;
        //check down minor diagonal
        for(int x = column - 1, y = row + 1; y < grid.length && x >= 0; x--, y++) {
            if (grid[y][x] == player)
                count ++;
            else
                break;
        }
        if(count >= 4)
            return true;
        //check up minor diagonal
        for(int x = column + 1, y = row - 1; y >= 0 && x < grid[0].length; x++, y--) {
            if (grid[y][x] == player)
                count ++;
            else
                break;
        }
        if(count >= 4)
            return true;
        return false;
    }
}