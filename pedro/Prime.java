import java.util.Scanner;

public class Prime {

    public static void main(String[] args){
        int number = get_number();

        if (checkPrime(number))
            System.out.println("It's a prime number");
        else
            System.out.println("It's not a prime number");

    }
    public static int get_number() {
        Scanner scanner = new Scanner(System.in);
        int number = 0;

        do {
            System.out.print("Insert a number: ");
            number = scanner.nextInt();
        } while(number == 0);

        return number;
    }

    public static boolean checkPrime(int num){
        if(num == 1)
            return false;

        for(int i=2;i<=num/2;i++){
            int temp=num%i;
            if(temp==0){
                return false;
            }
        }
        return true;
    }
}
