//Write a Java program to move all 0's to the end of an array. Maintain the relative order of the other (non-zero) array elements
//
//Write a Java program to get the difference between the largest and smallest values in an array of integers. The length of the array must be 1 and above

import java.util.Random;

public class Level6_array {
    static int SIZE = 10;

    public static void main(String[] args) {
        Random random = new Random();

        int[] array = new int[SIZE];
        for (int i = 0; i < SIZE; i++) //Fill the array
            array[i] = random.nextInt(5);

        System.out.println("Before: ");
        for (int num : array)
            System.out.printf("%d ", num);

        pushZeros(array);

        System.out.println("\n\nAfter: ");
        for (int num : array)
            System.out.printf("%d ", num);
    }

    public static void pushZeros(int array[]) {
        int count = 0;
        int length = array.length;

        for (int i = 0; i < length; i++)
            if (array[i] != 0)
                array[count++] = array[i];

        while (count < length)
            array[count++] = 0;
    }

}